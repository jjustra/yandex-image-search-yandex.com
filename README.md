# yandex-image-search-yandex.com

Firefox search addon for yandex.com.

## disclaimer

This addon is just a mod of original by Romkaq (rq-dev).

Singular change is removing RU version, leaving only EN.

You can get original here : https://addons.mozilla.org/en-US/firefox/addon/yandex-image-search-romkaq/
